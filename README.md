# tools-bitmanipulation-ts

This contains handy tools for manipulating bits, bytes and other low level stuff.

To include this in your project you must add the following line to your `.npmrc` file.
```
@secata-public:registry=https://gitlab.com/api/v4/packages/npm/
```
