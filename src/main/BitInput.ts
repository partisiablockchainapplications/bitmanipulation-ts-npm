import BN from "bn.js";

/**
 * Reader of an input stream. Able to read numbers with variable bit length. E.g. two numbers
 * followed by five booleans and then a BN is read from the stream perceived as a consecutive bit
 * array, the first 8 bytes is allocated to the two numbers, the next byte has five bits allocated
 * to booleans, the next 3 bits, plus the next 8 bytes is the BN.
 */
export class BitInput {
  private static readonly ZERO = new BN(0);
  private static readonly ONE = new BN(1);

  private readonly buffer: Buffer;
  private offset = 0;
  private bitIndex = 0;

  /**
   * Create a new BitInput that reads the buffer given.
   *
   * @param buffer the buffer to be read
   */
  constructor(buffer: Buffer) {
    this.buffer = buffer;
  }

  /**
   * Return a byte buffer of the specified length read from the next bytes.
   *
   * @param byteLength the number of bytes to read
   * @returns the read buffer
   */
  public readBytes(byteLength: number): Buffer {
    // Stryker disable next-line ConditionalExpression
    if (this.bitIndex === 0) {
      return this.readBytesBytewise(byteLength);
    } else {
      return this.readBytesBitwise(byteLength);
    }
  }

  /**
   * Reads the next 8 * byteLength bits one bit at a time and returns them as a byte array with
   * length byteLength.
   *
   * @param byteLength the number of bytes to read
   * @returns the read bytes
   */
  private readBytesBitwise(byteLength: number) {
    const buffer = Buffer.alloc(byteLength);
    for (let i = 0; i < byteLength; i++) {
      buffer[i] = this.readUnsignedNumber(8);
    }
    return buffer;
  }

  /**
   * Reads the next byteLength bytes from the stream and returns them as a byte array. Assumes that
   * bitIndex is 0 such that the read bytes matches the next bytes of the BitInput stream.
   *
   * @param byteLength the number of bytes to read
   * @returns the read bytes
   */
  private readBytesBytewise(byteLength: number) {
    if (this.offset + byteLength > this.buffer.length) {
      throw new Error("Reached end of buffer");
    }
    const buffer = this.buffer.slice(this.offset, this.offset + byteLength);
    this.offset += byteLength;
    return buffer;
  }

  /**
   * Read an unsigned BN of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @returns the BN created from the read bits
   */
  public readUnsignedBN(bitLength: number): BN {
    let sum = BitInput.ZERO;
    for (let i = 0; i < bitLength; i++) {
      if (this.readBoolean()) {
        sum = sum.add(BitInput.ONE.shln(i));
      }
    }
    return sum;
  }

  /**
   * Read an unsigned number of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @returns the number created from the read bits
   */
  public readUnsignedNumber(bitLength: number): number {
    if (bitLength > 53) {
      throw new Error(">53 bit numbers must be read using BitInput#readUnsignedBN");
    }
    return this.readUnsignedBN(bitLength).toNumber();
  }

  /**
   * Read a signed BN of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @returns the BN created from the read bits
   */
  public readSignedBN(bitLength: number): BN {
    const unsigned = this.readUnsignedBN(bitLength);
    if (unsigned.shrn(bitLength - 1).eqn(1)) {
      const max = BitInput.ONE.shln(bitLength);
      return unsigned.sub(max);
    }
    return unsigned;
  }

  /**
   * Read a signed number of the specified number of bits.
   *
   * @param bitLength the number of bits to read
   * @returns the number created from the read bits
   */
  public readSignedNumber(bitLength: number): number {
    if (bitLength > 53) {
      throw new Error(">53 bit numbers must be read using BitInput#readSignedBN");
    }
    return this.readSignedBN(bitLength).toNumber();
  }

  /**
   * Read the next boolean.
   *
   * @returns the read boolean
   */
  public readBoolean(): boolean {
    if (this.offset >= this.buffer.length) {
      throw new Error("Reached end of buffer");
    }
    const value = this.buffer[this.offset];
    const isBitSet = (value & (1 << this.bitIndex)) > 0;
    this.bitIndex++;
    if (this.bitIndex === 8) {
      this.bitIndex = 0;
      this.offset++;
    }
    return isBitSet;
  }
}
