import BN from "bn.js";

/**
 * Common interface of an endian-aware reader for an input stream.
 */
export interface ByteInput {
  /**
   * Reads a buffer of bytes.
   *
   * @param noBytes the number of bytes
   * @returns the read bytes
   */
  readBytes(noBytes: number): Buffer;

  /**
   * Reads a single byte from the stream as a boolean.
   *
   * @returns false if the read byte is 0 and true otherwise
   */
  readBoolean(): boolean;

  /**
   * Reads a signed 8-bit number from the stream.
   *
   * @returns the read number
   */
  readI8(): number;

  /**
   * Reads a signed 16-bit number from the stream.
   *
   * @returns the read number
   */
  readI16(): number;

  /**
   * Reads a signed 32-bit number from the stream.
   *
   * @returns the read number
   */
  readI32(): number;

  /**
   * Reads a signed 64-bit number from the stream.
   *
   * @returns the read number as a BN
   */
  readI64(): BN;

  /**
   * Reads a signed BN.
   *
   * @param noBytes the number of bytes to read
   * @returns the read BN from the stream
   */
  readSignedBigInteger(noBytes: number): BN;

  /**
   * Reads an unsigned 8-bit number from the stream.
   *
   * @returns the read number
   */
  readU8(): number;

  /**
   * Reads an unsigned 16-bit number from the stream.
   *
   * @returns the read number
   */
  readU16(): number;

  /**
   * Reads an unsigned 32-bit number from the stream.
   *
   * @returns the read number
   */
  readU32(): number;

  /**
   * Reads an unsigned 64-bit number from the stream.
   *
   * @returns the read number as a BN
   */
  readU64(): BN;

  /**
   * Reads an unsigned BN.
   *
   * @param noBytes the number of bytes to read
   * @returns the read BN from the stream
   */
  readUnsignedBigInteger(noBytes: number): BN;

  /**
   * Reads a string.
   *
   * @returns the read string
   */
  readString(): string;

  /**
   * Reads all remaining bytes to the end of the stream.
   *
   * @returns the rest of the stream as bytes
   */
  readRemaining(): Buffer;

  /**
   * Skip a number of bytes from this stream.
   *
   * @param numberOfBytes the number of bytes to skip
   * @throws Error if unable to skip the requested number of bytes
   */
  skipBytes(numberOfBytes: number): void;
}
