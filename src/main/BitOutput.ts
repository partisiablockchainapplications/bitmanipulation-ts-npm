import { BN } from "./index";

/** A bit array with the bits packed compactly in a byte array. */
export interface CompactBitArray {
  /** The content of the bit array store compactly in a buffer. */
  data: Buffer;
  /** The number of bits in the bit array. */
  length: number;
}

/**
 * Writer to an output stream. Able to write numbers with variable bit length. E.g. two numbers
 * followed by five booleans and then a BN is packed efficiently into the stream.
 */
export class BitOutput {
  private static readonly ONE = new BN(1);
  private static readonly ZERO = new BN(0);

  private index = 0;
  private readonly bitSet: number[] = [];

  /**
   * Creates a BitOutput, runs the serialization and returns the bits as a compact bit array.
   *
   * @param serializer the serialization to happen
   * @returns the serialized bits
   */
  public static serializeBits(serializer: (out: BitOutput) => void): CompactBitArray {
    const out = new BitOutput();
    serializer(out);
    const length = out.writtenBits();
    const data = out.toBuffer();
    return { data, length };
  }

  /**
   * Write a number of bytes from a byte buffer.
   *
   * @param buffer buffer containing the bytes to be written
   * @param offset starting offset of byte buffer
   * @param length number of bytes to write from offset
   * @returns the BitOutput with the written bytes
   */
  public writeBytes(buffer: Buffer, offset = 0, length = buffer.length): BitOutput {
    // Stryker disable next-line ConditionalExpression
    if (this.index === 0) {
      for (let i = 0; i < length; i++) {
        this.bitSet.push(buffer[i + offset]);
      }
    } else {
      for (let i = 0; i < length; i++) {
        this.writeNumberBits(buffer[i + offset], 8);
      }
    }
    return this;
  }

  /**
   * Write the specified number of bits of an unsigned BN to this builder.
   *
   * @param value the unsigned BN to write
   * @param bitLength the number of bits of the BN to write
   * @returns the BitOutput with the written BN
   */
  public writeUnsignedBN(value: BN, bitLength: number): BitOutput {
    BitOutput.isValueValid(value, bitLength);

    const fullBytes = bitLength >> 3;
    const asBuffer = value.toArrayLike(Buffer, "le", fullBytes + 1);
    this.writeBytes(asBuffer, 0, fullBytes);
    const remainingBits = bitLength - fullBytes * 8;
    this.writeNumberBits(asBuffer[fullBytes], remainingBits);

    return this;
  }

  /**
   * Write the specified number of bits of an unsigned number to this builder.
   *
   * @param value the unsigned number to write
   * @param bitLength the number of bits of the number to write
   * @returns the BitOutput with the written number
   */
  public writeUnsignedNumber(value: number, bitLength: number): BitOutput {
    if (bitLength > 53) {
      throw new Error(">53 bit numbers must be written using BitOutput#writeUnsignedBN");
    }
    return this.writeUnsignedBN(new BN(value), bitLength);
  }

  /**
   * Writes a number one bit at a time.
   *
   * @param value the number to be written
   * @param bitLength the number of bits to write
   */
  private writeNumberBits(value: number, bitLength: number) {
    for (let i = 0; i < bitLength; i++) {
      const isBitSet: boolean = (value & (1 << i)) !== 0;
      this.writeBoolean(isBitSet);
    }
    return this;
  }

  /**
   * Write the specified number of bits of a signed BN to this builder.
   *
   * @param value the signed BN to write
   * @param bitLength the number of bits of the BN to write
   * @returns the BitOutput with the written BN
   */
  public writeSignedBN(value: BN, bitLength: number): BitOutput {
    BitOutput.isSignedValueValid(value, bitLength);
    if (value.cmpn(0) === -1) {
      const max = BitOutput.ONE.shln(bitLength);
      return this.writeUnsignedBN(value.add(max), bitLength);
    }
    return this.writeUnsignedBN(value, bitLength);
  }

  /**
   * Write the specified number of bits of a signed number to this builder.
   *
   * @param value the signed number to write
   * @param bitLength the number of bits of the number to write
   * @returns the BitOutput with the written number
   */
  public writeSignedNumber(value: number, bitLength: number): BitOutput {
    if (bitLength > 53) {
      throw new Error(">53 bit numbers must be written using BitOutput#writeSignedBN");
    }
    return this.writeSignedBN(new BN(value), bitLength);
  }

  /**
   * Write the specified boolean to this builder.
   *
   * @param bool the boolean to write
   * @returns the BitOutput with the written boolean
   */
  public writeBoolean(bool: boolean): BitOutput {
    if (this.index === 0) {
      this.bitSet.push(0);
    }
    if (bool) {
      const bit = this.index;
      this.bitSet[this.bitSet.length - 1] = this.bitSet[this.bitSet.length - 1] | (1 << bit);
    }
    this.index++;
    if (this.index === 8) {
      this.index = 0;
    }
    return this;
  }

  /**
   * Gives the number of bits written so far.
   *
   * @returns the number of bits written
   */
  public writtenBits(): number {
    if (this.index === 0) {
      return this.bitSet.length * 8;
    } else {
      return this.bitSet.length * 8 - (8 - this.index);
    }
  }

  /**
   * Create a Buffer from the written values.
   *
   * @returns the created Buffer
   */
  public toBuffer(): Buffer {
    return Buffer.from(this.bitSet);
  }

  private static isValueValid(value: BN, bitLength: number) {
    const max = BitOutput.ONE.shln(bitLength).sub(BitOutput.ONE);
    if (value.ltn(0) || value.gt(max)) {
      throw new Error(value + " cannot be represented as an unsigned " + bitLength + " bit number");
    }
    return max;
  }

  private static isSignedValueValid(value: BN, bitLength: number) {
    const max = BitOutput.ONE.shln(bitLength - 1).sub(BitOutput.ONE);
    const min = BitOutput.ZERO.sub(BitOutput.ONE.shln(bitLength - 1));
    if (value.gt(max) || value.lt(min)) {
      throw new Error(value + " cannot be represented as a signed " + bitLength + " bit number");
    }
    return max;
  }
}
