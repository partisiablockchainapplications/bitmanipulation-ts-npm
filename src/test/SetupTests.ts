import { FetchMock, GlobalWithFetchMock } from "jest-fetch-mock";

const customGlobal: GlobalWithFetchMock = global as unknown as GlobalWithFetchMock;
// eslint-disable-next-line
customGlobal.fetch = require("jest-fetch-mock") as FetchMock;
customGlobal.fetchMock = customGlobal.fetch;
