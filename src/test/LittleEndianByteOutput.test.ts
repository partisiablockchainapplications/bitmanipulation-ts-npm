import { LittleEndianByteOutput, BN } from "../main";

test("write I64", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeI64(new BN("0706050403020100", "hex"));
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U64", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeU64(new BN("0706050403020100", "hex"));
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write I32", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeI32(0x03020100);
  writer.writeI32(0x07060504);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U32", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeU32(0x03020100);
  writer.writeU32(0x07060504);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write I16", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeI16(0x0100);
  writer.writeI16(0x0302);
  writer.writeI16(0x0504);
  writer.writeI16(0x0706);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U16", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeU16(0x0100);
  writer.writeU16(0x0302);
  writer.writeU16(0x0504);
  writer.writeU16(0x0706);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write I8", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeI8(0x00);
  writer.writeI8(0x01);
  writer.writeI8(0x02);
  writer.writeI8(0x03);
  writer.writeI8(0x04);
  writer.writeI8(0x05);
  writer.writeI8(0x06);
  writer.writeI8(0x07);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U8", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeU8(0x00);
  writer.writeU8(0x01);
  writer.writeU8(0x02);
  writer.writeU8(0x03);
  writer.writeU8(0x04);
  writer.writeU8(0x05);
  writer.writeU8(0x06);
  writer.writeU8(0x07);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write bytes", () => {
  const writer = new LittleEndianByteOutput();
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  writer.writeBytes(expected);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write bool", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeBoolean(false);
  writer.writeBoolean(true);
  const expected = Buffer.from([0x00, 0x01]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write big integer positive", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeSignedBigInteger(new BN("16"), 2);
  const expected = Buffer.from([0x10, 0x00]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write big integer negative", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeSignedBigInteger(new BN("-16"), 2);
  const expected = Buffer.from([-0x10, -1]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write signed big integer", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeSignedBigInteger(new BN("0F0E0D0C0B0A09080706050403020100", "hex"), 16);
  const expected = Buffer.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write unsigned big integer", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeUnsignedBigInteger(new BN("0F0E0D0C0B0A09080706050403020100", "hex"), 16);
  const expected = Buffer.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write too large big integer", () => {
  const writer = new LittleEndianByteOutput();
  expect(() => writer.writeSignedBigInteger(new BN(257), 1)).toThrowError(
    "byte array longer than desired length"
  );
});

test("write negative unsigned big integer", () => {
  const writer = new LittleEndianByteOutput();
  expect(() => writer.writeUnsignedBigInteger(new BN(-1), 4)).toThrowError(
    "Value must be non negative"
  );
});

test("write string", () => {
  const writer = new LittleEndianByteOutput();
  writer.writeString("ABC BB");
  const expected = Buffer.from([6, 0, 0, 0, 65, 66, 67, 32, 66, 66]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("serialize", () => {
  const actual = LittleEndianByteOutput.serialize((out) => out.writeI16(0x42));
  expect(actual).toEqual(Buffer.from("4200", "hex"));
});
