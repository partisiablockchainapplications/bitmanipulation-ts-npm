import { BitOutput } from "../main";
import { BitInput } from "../main";
import BN from "bn.js";
import { BigEndianByteOutput } from "../main";

test("read unsigned number", () => {
  const buffer = Buffer.from([0xf0, 0x05, 0xff]);

  const bitReader = new BitInput(buffer);

  expect(bitReader.readUnsignedNumber(12)).toEqual(0x5f0);
  expect(bitReader.readUnsignedNumber(8)).toEqual(0xf0);
  expect(bitReader.readUnsignedNumber(4)).toEqual(0xf);
});

test("read signed number", () => {
  const buffer = Buffer.from([0xf0, 0x05, 0xff]);

  const bitReader = new BitInput(buffer);

  expect(bitReader.readSignedNumber(12)).toEqual(0x5f0);
  expect(bitReader.readSignedNumber(8)).toEqual(-16);
  expect(bitReader.readSignedNumber(4)).toEqual(-1);
});

test("write unsigned number", () => {
  const writer = new BitOutput();
  writer.writeUnsignedNumber(0x5f0, 12);
  writer.writeUnsignedNumber(0xf0, 8);
  writer.writeUnsignedNumber(0xf, 4);

  expect(writer.toBuffer()).toEqual(Buffer.from([0xf0, 0x05, 0xff]));
});

test("write signed number", () => {
  const writer = new BitOutput();
  writer.writeSignedNumber(0x5f0, 12);
  writer.writeSignedNumber(-16, 8);
  writer.writeSignedNumber(-1, 4);

  expect(writer.toBuffer()).toEqual(Buffer.from([0xf0, 0x05, 0xff]));
});

test("boolean", () => {
  const booleans = [true, true, false, true, false, false, true, true, false];

  const writer = new BitOutput();
  for (const bool of booleans) {
    writer.writeBoolean(bool);
  }

  const reader = new BitInput(writer.toBuffer());
  for (const bool of booleans) {
    expect(reader.readBoolean()).toEqual(bool);
  }
});

test("unsigned number", () => {
  const writer = new BitOutput()
    .writeUnsignedNumber(42, 31)
    .writeUnsignedNumber(9007199254740991, 53)
    .writeUnsignedNumber(5, 5)
    .writeUnsignedNumber(1, 1)
    .writeUnsignedNumber(0, 32)
    .writeUnsignedNumber(3, 17);

  const reader = new BitInput(writer.toBuffer());
  expect(reader.readUnsignedNumber(31)).toEqual(42);
  expect(reader.readUnsignedNumber(53)).toEqual(9007199254740991);
  expect(reader.readUnsignedNumber(5)).toEqual(5);
  expect(reader.readUnsignedNumber(1)).toEqual(1);
  expect(reader.readUnsignedNumber(32)).toEqual(0);
  expect(reader.readUnsignedNumber(17)).toEqual(3);
});

test("unsigned BN", () => {
  const writer = new BitOutput()
    .writeUnsignedBN(new BN("256"), 9)
    .writeUnsignedBN(new BN("999999999999999999999"), 5498)
    .writeUnsignedBN(new BN("0"), 9)
    .writeUnsignedBN(new BN("4294967295"), 32)
    .writeUnsignedBN(new BN("4294967296"), 33);

  const reader = new BitInput(writer.toBuffer());
  expect(reader.readUnsignedBN(9)).toEqual(new BN("256"));
  expect(reader.readUnsignedBN(5498)).toEqual(new BN("999999999999999999999"));
  expect(reader.readUnsignedBN(9)).toEqual(new BN("0"));
  expect(reader.readUnsignedBN(32)).toEqual(new BN("4294967295"));
  expect(reader.readUnsignedBN(33)).toEqual(new BN("4294967296"));
});

test("signed number", () => {
  const writer = new BitOutput()
    .writeSignedNumber(-42, 31)
    .writeSignedNumber(-4503599627370496, 53)
    .writeSignedNumber(5, 5)
    .writeSignedNumber(-1, 1)
    .writeSignedNumber(0, 32)
    .writeSignedNumber(-3, 17);

  const reader = new BitInput(writer.toBuffer());
  expect(reader.readSignedNumber(31)).toEqual(-42);
  expect(reader.readSignedNumber(53)).toEqual(-4503599627370496);
  expect(reader.readSignedNumber(5)).toEqual(5);
  expect(reader.readSignedNumber(1)).toEqual(-1);
  expect(reader.readSignedNumber(32)).toEqual(0);
  expect(reader.readSignedNumber(17)).toEqual(-3);
});

test("signed BN", () => {
  const writer = new BitOutput()
    .writeSignedBN(new BN("256"), 10)
    .writeSignedBN(new BN("-999999999999999999999"), 5498)
    .writeSignedBN(new BN("0"), 9)
    .writeSignedBN(new BN("4294967295"), 33)
    .writeSignedBN(new BN("-4294967296"), 33);

  const reader = new BitInput(writer.toBuffer());
  expect(reader.readSignedBN(10)).toEqual(new BN("256"));
  expect(reader.readSignedBN(5498).eq(new BN("-999999999999999999999"))).toBeTruthy();
  expect(reader.readSignedBN(9)).toEqual(new BN("0"));
  expect(reader.readSignedBN(33)).toEqual(new BN("4294967295"));
  expect(reader.readSignedBN(33)).toEqual(new BN("-4294967296"));
});

test("buffer", () => {
  const bufferWriter = new BigEndianByteOutput();
  bufferWriter.writeI64(new BN("34956034"));
  bufferWriter.writeBytes(Buffer.alloc(9));
  bufferWriter.writeString("abababab");
  bufferWriter.writeBoolean(true);
  bufferWriter.writeI32(9955);
  const buffer = bufferWriter.toBuffer();

  const writer = new BitOutput().writeBytes(buffer);

  const reader = new BitInput(writer.toBuffer());
  expect(reader.readBytes(buffer.length)).toEqual(buffer);
});

test("buffer offset", () => {
  const bufferWriter = new BigEndianByteOutput();
  bufferWriter.writeI64(new BN("34956034"));
  bufferWriter.writeBytes(Buffer.alloc(9));
  bufferWriter.writeString("abababab");
  bufferWriter.writeBoolean(true);
  bufferWriter.writeI32(9955);
  const buffer = bufferWriter.toBuffer();

  const writer = new BitOutput().writeBoolean(true).writeBytes(buffer);

  const reader = new BitInput(writer.toBuffer());
  expect(reader.readBoolean()).toEqual(true);
  expect(reader.readBytes(buffer.length)).toEqual(buffer);
});

test("invalid numbers", () => {
  const writer = new BitOutput();
  expect(() => writer.writeUnsignedBN(new BN(3), 1)).toThrowError(
    "3 cannot be represented as an unsigned 1 bit number"
  );
  expect(() => writer.writeUnsignedNumber(3, 1)).toThrowError(
    "3 cannot be represented as an unsigned 1 bit number"
  );
  expect(() => writer.writeUnsignedBN(new BN(-1), 2)).toThrowError(
    "-1 cannot be represented as an unsigned 2 bit number"
  );
  expect(() => writer.writeUnsignedNumber(-1, 2)).toThrowError(
    "-1 cannot be represented as an unsigned 2 bit number"
  );
  expect(() => writer.writeUnsignedNumber(3, 54)).toThrowError(
    ">53 bit numbers must be written using BitOutput#writeUnsignedBN"
  );

  expect(() => writer.writeSignedBN(new BN(3), 2)).toThrowError(
    "3 cannot be represented as a signed 2 bit number"
  );
  expect(() => writer.writeSignedBN(new BN(-3), 2)).toThrowError(
    "-3 cannot be represented as a signed 2 bit number"
  );
  expect(() => writer.writeSignedNumber(3, 2)).toThrowError(
    "3 cannot be represented as a signed 2 bit number"
  );
  expect(() => writer.writeSignedNumber(-3, 2)).toThrowError(
    "-3 cannot be represented as a signed 2 bit number"
  );
  expect(() => writer.writeSignedNumber(3, 54)).toThrowError(
    ">53 bit numbers must be written using BitOutput#writeSignedBN"
  );

  const reader = new BitInput(Buffer.alloc(0));
  expect(() => reader.readUnsignedNumber(54)).toThrowError(
    ">53 bit numbers must be read using BitInput#readUnsignedBN"
  );
  expect(() => reader.readSignedNumber(54)).toThrowError(
    ">53 bit numbers must be read using BitInput#readSignedBN"
  );
  expect(() => reader.readBoolean()).toThrowError("Reached end of buffer");
  expect(() => reader.readBytes(1)).toThrowError("Reached end of buffer");
});

test("write different types", () => {
  const writer = new BigEndianByteOutput();
  writer.writeString("abababab");
  writer.writeI32(9955);
  const buffer = writer.toBuffer();

  const bits = BitOutput.serializeBits((out) => {
    out
      .writeBoolean(true)
      .writeUnsignedNumber(12, 25)
      .writeUnsignedBN(new BN("94857694857649856749867"), 240)
      .writeSignedNumber(15, 5)
      .writeBoolean(false)
      .writeBytes(buffer)
      .writeSignedBN(new BN("1234567890"), 63)
      .writeBoolean(true)
      .writeUnsignedNumber(255, 8);
  });
  expect(bits.length).toEqual(1 + 25 + 240 + 5 + 1 + buffer.length * 8 + 63 + 1 + 8);

  const reader = new BitInput(bits.data);
  expect(reader.readBoolean()).toEqual(true);
  expect(reader.readUnsignedNumber(25)).toEqual(12);
  expect(reader.readUnsignedBN(240)).toEqual(new BN("94857694857649856749867"));
  expect(reader.readSignedNumber(5)).toEqual(15);
  expect(reader.readBoolean()).toEqual(false);
  expect(reader.readBytes(buffer.length)).toEqual(buffer);
  expect(reader.readSignedBN(63)).toEqual(new BN("1234567890"));
  expect(reader.readBoolean()).toEqual(true);
  expect(reader.readUnsignedNumber(8)).toEqual(255);
  expect(() => reader.readBoolean()).toThrowError("Reached end of buffer");
});

test("written bits", () => {
  const writer = new BitOutput().writeBoolean(true);
  expect(writer.writtenBits()).toEqual(1);
  writer.writeUnsignedNumber(98, 7);
  expect(writer.writtenBits()).toEqual(1 + 7);
  writer.writeBoolean(true);
  expect(writer.writtenBits()).toEqual(1 + 7 + 1);
  writer.writeUnsignedBN(new BN(348957), 29);
  expect(writer.writtenBits()).toEqual(1 + 7 + 1 + 29);
});

test("part of a byte array", () => {
  const buffer = Buffer.from([1, 2, 3, 4, 5, 6, 7]);
  const writer = new BitOutput()
    .writeBytes(buffer, 2, 3)
    .writeBoolean(true)
    .writeBytes(buffer, 2, 3);

  const reader = new BitInput(writer.toBuffer());
  const expected = Buffer.from([3, 4, 5]);
  expect(reader.readBytes(3)).toEqual(expected);
  expect(reader.readBoolean()).toEqual(true);
  expect(reader.readBytes(3)).toEqual(expected);
});
