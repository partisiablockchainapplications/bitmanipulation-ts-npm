import { BigEndianByteOutput, BN } from "../main";

test("write I64", () => {
  const writer = new BigEndianByteOutput();
  writer.writeI64(new BN("0001020304050607", "hex"));
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U64", () => {
  const writer = new BigEndianByteOutput();
  writer.writeU64(new BN("0001020304050607", "hex"));
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write I32", () => {
  const writer = new BigEndianByteOutput();
  writer.writeI32(0x00010203);
  writer.writeI32(0x04050607);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U32", () => {
  const writer = new BigEndianByteOutput();
  writer.writeU32(0x00010203);
  writer.writeU32(0x04050607);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write I16", () => {
  const writer = new BigEndianByteOutput();
  writer.writeI16(0x0001);
  writer.writeI16(0x0203);
  writer.writeI16(0x0405);
  writer.writeI16(0x0607);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U16", () => {
  const writer = new BigEndianByteOutput();
  writer.writeU16(0x0001);
  writer.writeU16(0x0203);
  writer.writeU16(0x0405);
  writer.writeU16(0x0607);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write I8", () => {
  const writer = new BigEndianByteOutput();
  writer.writeI8(0x00);
  writer.writeI8(0x01);
  writer.writeI8(0x02);
  writer.writeI8(0x03);
  writer.writeI8(0x04);
  writer.writeI8(0x05);
  writer.writeI8(0x06);
  writer.writeI8(0x07);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write U8", () => {
  const writer = new BigEndianByteOutput();
  writer.writeU8(0x00);
  writer.writeU8(0x01);
  writer.writeU8(0x02);
  writer.writeU8(0x03);
  writer.writeU8(0x04);
  writer.writeU8(0x05);
  writer.writeU8(0x06);
  writer.writeU8(0x07);
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write bytes", () => {
  const writer = new BigEndianByteOutput();
  const expected = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  writer.writeBytes(expected);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write bool", () => {
  const writer = new BigEndianByteOutput();
  writer.writeBoolean(false);
  writer.writeBoolean(true);
  const expected = Buffer.from([0x00, 0x01]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write big integer positive", () => {
  const writer = new BigEndianByteOutput();
  writer.writeSignedBigInteger(new BN("16"), 2);
  const expected = Buffer.from([0x00, 0x10]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write big integer negative", () => {
  const writer = new BigEndianByteOutput();
  writer.writeSignedBigInteger(new BN("-16"), 2);
  const expected = Buffer.from([-1, -0x10]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write signed big integer", () => {
  const writer = new BigEndianByteOutput();
  writer.writeSignedBigInteger(new BN("000102030405060708090A0B0C0D0E0F", "hex"), 16);
  const expected = Buffer.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write unsigned big integer", () => {
  const writer = new BigEndianByteOutput();
  writer.writeUnsignedBigInteger(new BN("000102030405060708090A0B0C0D0E0F", "hex"), 16);
  const expected = Buffer.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("write too large big integer", () => {
  const writer = new BigEndianByteOutput();
  expect(() => writer.writeSignedBigInteger(new BN(257), 1)).toThrowError(
    "byte array longer than desired length"
  );
});

test("write negative unsigned big integer", () => {
  const writer = new BigEndianByteOutput();
  expect(() => writer.writeUnsignedBigInteger(new BN(-1), 4)).toThrowError(
    "Value must be non negative"
  );
});

test("write string", () => {
  const writer = new BigEndianByteOutput();
  writer.writeString("ABC BB");
  const expected = Buffer.from([0, 0, 0, 6, 65, 66, 67, 32, 66, 66]);
  expect(writer.toBuffer()).toEqual(expected);
});

test("serialize", () => {
  const actual = BigEndianByteOutput.serialize((out) => out.writeI16(0x42));
  expect(actual).toEqual(Buffer.from("0042", "hex"));
});
